require 'spec_helper'
require 'sidekiq/testing'

RSpec.describe ExportWorker, type: :worker do
  let(:admin) { create(:user, :admin, :active) }
  let(:export_service) { ExportService }
  let(:upload_files_service) { UploadFilesService }

  before { Sidekiq::Worker.clear_all }

  context 'worker settings' do
    it { is_expected.to be_processed_in :export }
    it { is_expected.to be_retryable 2 }
  end

  context 'with correct params' do
    it 'should start worker' do
      allow_any_instance_of(export_service).to receive(:call)
      allow_any_instance_of(upload_files_service).to receive(:call)

      expect { ExportWorker.perform_async(admin.id, 'User', 'csv', 'users') }
        .to change(ExportWorker.jobs, :size).by(1)
    end
  end
end
