source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.1'

gem 'aruba'
gem 'axlsx_rails'
gem 'bootstrap'
gem 'bullet'
gem 'country_select'
gem 'devise'
gem 'devise-bootstrap-views'
gem 'dotenv-rails'
gem 'faraday'
gem 'faraday_middleware'
gem 'google-cloud-storage'
gem 'rails-bootstrap'
gem 'rubyzip', '>= 1.2.1'
gem 'roo'
gem "interactor"
gem "interactor-rails", "~> 2.0"
gem 'sidekiq', '~>6.0.0'
gem 'pagy'
gem 'pundit'
gem 'pdf-reader'
gem 'prawn'
gem 'jsonapi-serializer'
gem 'jwt'
gem 'hiredis'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'rails', '~> 6.1.3', '>= 6.1.3.2'
# Use postgresql as the database for Active Record
gem 'pg', '~> 1.1'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
gem 'jquery-turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

group :development, :test do
  gem 'annotate'
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capybara', '>= 3.26'
  gem 'database_cleaner'
  gem 'database_cleaner-active_record'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'rails-controller-testing'
  gem 'rubocop'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'
  gem 'rspec-rails'
  gem 'pry'
end

group :development do
  gem 'annotate'
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'listen', '~> 3.3'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'capybara-selenium', '~> 0.0.6'
  gem 'selenium-webdriver'
  gem 'simplecov'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'rspec-sidekiq'
  gem 'webdrivers'
  gem 'webmock'
  gem 'vcr'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'twitter-bootstrap-rails'
gem 'simple_form'
gem 'slim-rails'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'popper_js'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-rails_csrf_protection'