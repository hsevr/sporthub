# frozen_string_literal: true

require 'spec_helper'

describe 'ApplicationApiError' do
  describe '#const_missing' do
    context 'when exceptions are declared  are in localization file' do
      it 'should raise UserNotFound to ApplicationApiError' do
        I18n.t('error').keys.each do |e|
          expect {
            raise ApplicationApiError.const_get(e.to_s.camelcase)
          }.to raise_error(ApplicationApiError)
        end
      end
    end

    context 'when exceptions are not declared  are in localization file' do
      it 'should raise NameError' do
        expect {
          raise ApplicationApiError::NotExistException
        }.to raise_error(NameError)
      end
    end
  end
end