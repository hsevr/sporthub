# frozen_string_literal: true

require 'csv'

class ExporterCSV < BaseExporter
  def export(data, file_location, params = {})
    CSV.open(file_location, "wb") do |csv|
      csv << data.export_attributes

      data.find_each do |row|
        csv << data.export_attributes.map{ |attr| row.public_send(attr) }
      end
    end
  end
end