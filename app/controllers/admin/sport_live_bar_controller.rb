# frozen_string_literal: true

module Admin
  # Sport live bar controller
  class SportLiveBarController < BaseController

    def teams
      sport_hub_api.params = request_params
      @teams = sport_hub_api.get_teams

      render json: @teams, status: :ok
    end

    def players
      sport_hub_api.params = request_params
      @players = sport_hub_api.get_players

      render json: @players, status: :ok
    end

    private

    def request_params
      params.permit(
        :id,
        :season,
      )
    end

    def sport_hub_api
      @sport_hub_api ||= Externals::SportHub.new
    end
  end
end