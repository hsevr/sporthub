# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Users::SessionsController, type: :controller do
  let(:user) { create(:user) }

  context 'Session#create' do
    it 'Sign in' do
      sign_in user
      expect(controller.current_user).to eq(user)
    end

    it 'Sign in with invalid credentials' do
      wrong_user = user
      wrong_user.password = 'password123'

      sign_in wrong_user

      expect(controller.current_user).to eq(nil)
    end

    it 'Sign in and log out' do
      sign_in user
      expect(controller.current_user).to eq(user)
      sign_out user
      expect(controller.current_user).to eq(nil)
    end
  end
end
