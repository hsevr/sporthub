class ExportMailer < ApplicationMailer
  default from: 'sportHub@sportHub.com'

  def export(user, file_location, file_type)
    @user = user
    @title = "Export"
    subject = "Your Export is Ready"
    attachments["attachment.#{file_type}"] = File.read(file_location)
    mail(:to => user.email, :subject => subject)
  end
end