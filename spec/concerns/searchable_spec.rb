# frozen_string_literal: true

require 'spec_helper'

describe 'Searchable concern' do
  let(:user) { create(:user, :admin, :active) }

  describe '#search_by_fields' do
    context 'with record' do
      it 'returns the desired entry' do
        expect(User.search_by_fields(user.email, %i[email first_name last_name]).first.email)
          .to eq(user.email)
      end

      it 'returns empty array' do
        expect(User.search_by_fields(user.email, %i[last_name]))
          .to eq([])
      end
    end
  end
end