# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Admin::SubCategoriesController, type: :controller do
  let(:user) { create(:user, :admin, :active) }
  let(:category) { create(:category, :show) }
  let(:sub_category) { create(:sub_category, :show, category: category) }
  let(:sub_category_attributes) { attributes_for(:sub_category, category_id: category) }

  before do
    sign_in user
  end

  describe '#create' do
    context 'with valid sub_category params' do
      it 'creates a new sub_category' do
        post :create, params: { admin_sub_category: sub_category_attributes }
        expect(Admin::SubCategory.exists?(assigns(:sub_category).id)).to eq(true)
      end
    end

    context 'with invalid params' do
      it 'raise error' do
        expect { post :create }.to raise_error(ActionController::ParameterMissing)
      end
    end
  end

  describe '#update' do
    context 'with valid params' do
      it 'Update the category' do
        patch :update, params: { id: sub_category, admin_sub_category: { name: 'new name' } }
        expect(assigns(:sub_category).name).to eq('new name')
      end
    end
    context 'with invalid params' do
      it 'raise_error record not found' do
        expect { patch :update, params: { id: 123, admin_sub_category: { name: 'new name' } } }.
          to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe '#destroy' do
    context 'with valid params' do
      it 'Destroy the category' do
        delete :destroy, params: { id: sub_category }
        expect(Admin::SubCategory.exists?(sub_category.id)).to eq(false)
      end
    end
    context 'with invalid params' do
      it 'raise_error record not found' do
        expect { delete :destroy, params: { id: 1 } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
