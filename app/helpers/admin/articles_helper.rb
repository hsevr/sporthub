# frozen_string_literal: true

module Admin
  module ArticlesHelper
    def get_reverse_article_status(article)
      article.published? ? 'unpublish' : 'published'
    end
  end
end
