# frozen_string_literal: true

module UrlHelper
  def current_page_params(param_for: 'link', except: [])
    case param_for
    when 'link'
      request.params.slice(*permitted_current_params)
    when 'form'
      fields = ''
      request.query_parameters.collect do |key, value|
        if (except.include?(key))
          next
        end

        fields += hidden_field_tag key, value
      end

      fields.html_safe
    end
  end

  def get_page_params
    params
  end

  private

  def permitted_current_params
    %w[q order order_by filter selected_user tab category sub_category team]
  end
end