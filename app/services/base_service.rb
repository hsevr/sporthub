# frozen_string_literal: true

# Base Service
class BaseService
  def self.call(*args, &block)
    new(*args, &block).call
  end
end