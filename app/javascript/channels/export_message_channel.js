import consumer from "./consumer"

consumer.subscriptions.create("ExportMessageChannel", {
  connected() {
  },

  disconnected() {
  },

  received(data) {
    $('#export-message').html(data.html)
    $('#export-message').show()
  }
});
