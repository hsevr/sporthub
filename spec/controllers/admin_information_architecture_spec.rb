# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Admin::InformationArchitectureController, type: :controller do
  let(:user) { create(:user, :admin, :active) }
  let(:category) { create(:category, :show) }
  let(:sub_category) { create(:sub_category, :show, category: category) }
  let(:team) { create(:team, :show, sub_category: sub_category) }

  before do
    sign_in user
  end

  describe '#index' do

    before do
      category
      get :index
    end

    context 'without params' do
      it 'should return all categories' do
        expect(assigns(:categories)).to eq(Admin::Category.all)
      end
    end

    context 'with category' do

      before do
        sub_category
        get :index, params: { category: category }
      end

      it 'should return sub_category' do
        expect(assigns(:sub_categories)).to eq(Admin::SubCategory.where(category_id: controller.params[:category]))
      end

      it 'should return category' do
        expect(assigns(:category)).to eq(controller.params[:category])
      end
    end

    context 'with category and sub_category' do

      before do
        team
        get :index, params: { category: category, sub_category: sub_category }
      end

      it 'should return sub_category' do
        expect(assigns(:teams)).to eq(Admin::Team.where(sub_category_id: controller.params[:sub_category]))
      end

      it 'should return category' do
        expect(assigns(:sub_category)).to eq(controller.params[:sub_category])
      end
    end
  end
end
