# frozen_string_literal: true

require 'axlsx'

#ExporterXLS
class ExporterXLSX < BaseExporter
  def export(data, file_location, params = {})
    Axlsx::Package.new do |p|
      p.workbook.add_worksheet(:name => "Export") do |sheet|
        attrs = data.export_attributes

        sheet.add_row attrs.map { |n| data.first.class.human_attribute_name(n) }

        data.each do |row|
          sheet.add_row attrs.map { |a| row.public_send(a) }
        end
      end

      p.serialize(file_location)
    end
  end
end