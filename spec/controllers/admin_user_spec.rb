# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Admin::UsersController, type: :controller do
  let(:user) { create(:user, :admin, :active) }
  let(:user_client) { create(:user, :client, :active) }
  let(:user_client_blocked) { create(:user, :client, :blocked) }

  before do
    sign_in user
  end

  context 'User#index' do
    before { get :index }

    it 'should return admin users' do
      expect(assigns(:admin_users)).to eq(User.admin)
    end

    it 'should return default admin user' do
      expect(assigns(:selected_user)).to eq(User.admin.first)
    end

    it 'should return client users' do
      user_client
      expect(assigns(:simple_users)).to eq(User.simple_user)
    end

    it 'should return default user' do
      expect(assigns(:selected_user)).to eq(User.admin.first)
    end

    it 'should return default tab' do
      expect(assigns(:tab)).to eq('users')
    end
  end

  context 'User#delete' do
    it 'delete active users' do
      delete :destroy, params: { id: user_client.id }
      expect(User.exists?(user_client.id)).to eq(false)
    end

    it 'delete blocked users' do
      blocked_user = create(:user, :blocked)

      delete :destroy, params: { id: blocked_user.id }
      expect(User.exists?(blocked_user.id)).to eq(false)
    end
  end

  context 'User#change_status' do
    it 'block an active user' do
      patch :change_status, params: { id: user_client, user: { status: 'blocked' } }
      expect(assigns(:user).blocked?).to eq(true)
    end

    it 'Shouldn`t block an active admin user' do
      patch :change_status, params: { id: user, user: { status: 'blocked' } }
      expect(assigns(:user).blocked?).to eq(false)
    end

    it 'active a blocked user' do
      patch :change_status, params: { id: user_client_blocked, user: { status: 'active' } }
      expect(assigns(:user).active?).to eq(true)
    end

    it 'Change personal info' do
      attr = { first_name: 'newName', last_name: 'NewLastName', email: 'test@new.com' }

      post :update, params: { id: user.id, user: attr }
      expect(assigns(:user)).to eq(user)
    end
  end
end
