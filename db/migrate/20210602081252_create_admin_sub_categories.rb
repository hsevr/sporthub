class CreateAdminSubCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :sub_categories, id: :uuid do |t|
      t.string :name
      t.belongs_to :category, null: false, foreign_key: true, type: :uuid
      t.integer :order, default: 0
      t.boolean :is_hidden, default: false

      t.timestamps
    end

    add_index :sub_categories, :name
  end
end
