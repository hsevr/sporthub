require 'spec_helper'

describe 'SportHubApi' do
  let(:sport_hub_api) { Externals::SportHub.new }

  let(:player_params) { { id: 33, season: 2019 } }
  let(:team_params) { { id: 1 } }
  let(:teams_responce) do
    VCR.use_cassette("sport_hub/teams") { sport_hub_api.get_teams }
  end
  let(:teams_error_responce) do
    VCR.use_cassette("sport_hub/teams_error") { sport_hub_api.get_teams }
  end
  let(:players_responce) do
    VCR.use_cassette("sport_hub/players") { sport_hub_api.get_players }
  end
  let(:players_error_responce) do
    VCR.use_cassette("sport_hub/players_error") { sport_hub_api.get_players }
  end

  describe "#players" do
    context 'check response data' do
      before do
        sport_hub_api.params = player_params
      end

      it "should return correct data" do
        expect(players_responce).to be_kind_of(Hash)
        expect(players_responce['errors']).to eq([])
        expect(players_responce['get']).to eq('players')
        expect(players_responce['response']).to_not eq([])
      end
    end

    context 'check errors cases' do
      it "should return error" do
        expect(players_error_responce).to be_kind_of(Hash)
        expect(players_error_responce['errors']['required']).to eq("At least one parameter is required.")
        expect(players_error_responce['response']).to eq([])
      end
    end
  end

  describe "#teams" do
    context 'check response data' do
      before do
        sport_hub_api.params = team_params
      end

      it "should return correct data" do
        expect(teams_responce).to be_kind_of(Hash)
        expect(teams_responce['errors']).to eq([])
        expect(teams_responce['get']).to eq('teams')
        expect(teams_responce['response']).to_not eq([])
      end
    end

    context 'check errors cases' do
      it "should return error" do
        expect(teams_error_responce).to be_kind_of(Hash)
        expect(teams_error_responce['errors']['required']).to eq("At least one parameter is required.")
        expect(teams_error_responce['response']).to eq([])
      end
    end
  end
end