# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users, id: :uuid do |t|
      t.string :first_name,         null: false
      t.string :last_name,          null: false
      t.string :email,              null: false
      t.string :encrypted_password, null: false

      t.datetime :remember_created_at

      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      t.integer :status, default: 1, limit: 1
      t.boolean :is_admin, default: false
      t.timestamps default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :users, :email,                unique: true
  end
end
