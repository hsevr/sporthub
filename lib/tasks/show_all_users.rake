task :show_all_users, [:batch_size] => :environment do |t, args|
  User.find_in_batches(batch_size: args[:batch_size].to_i || 500) do |users|
    users.each do |user|
      puts "User: #{user.full_name}, email: #{user.email}"
    end

    puts '----------------------'
  end
end
