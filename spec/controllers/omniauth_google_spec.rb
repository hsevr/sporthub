# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Users::OmniauthCallbacksController, type: :controller do
  let(:user) { create(:user) }

  before do
    request.env['devise.mapping'] = Devise.mappings[:user]
    request.env['omniauth.auth'] = OmniAuth::AuthHash.new({
                                                            provider: 'google_oauth2',
                                                            uid: '987654231',
                                                            info: {
                                                              name: 'Tony Stark',
                                                              first_name: 'Alberto',
                                                              last_name: 'Pellizzon',
                                                              email: 'tonya@stark.com'
                                                            },
                                                            credentials: {
                                                              token: 'token',
                                                              refresh_token: 'refresh token'
                                                            }
                                                          })
  end

  context 'OmniauthCallbacks#google_oauth2' do
    it 'Sign in by Google+' do
      get :google_oauth2
      expect(controller.current_user).to have_attributes(email: 'tonya@stark.com', uid: '987654231')
    end
  end
end
