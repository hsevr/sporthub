# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Api::V1::CategoriesController, type: :controller do
  let(:user) { create(:user, :admin, :active) }
  let(:category) { create(:category, :show) }

  before do
    request.headers.merge!(authenticated_header(user))
  end

  describe '#index' do
    before do
      category
      get :index
    end

    context 'when categories exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all article items' do
        expect(json['data']).not_to be_empty
        expect(json['data'].size).to eq(1)
      end
    end
  end

  describe '#show' do
    context 'when the record exists' do
      before do
        get :show, params: { id: category }
      end
      it 'returns the category' do
        expect(json['data']).not_to be_empty
        expect(json['data']['id']).to eq(category.id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      before do
        get :show, params: { id: '777' }
      end

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Admin::Category/)
      end
    end
  end
end