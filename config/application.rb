require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Sporthub
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
    # config.autoload_paths += Dir["#{config.root}/app/repositories/**/"]
    # config.autoload_paths += Dir["#{config.root}/app/services/**/"]
    # config.autoload_paths += %w(
    #   #{config.root}/app/jobs
    #   #{config.root}/app/services
    #   #{config.root}/app/repositories
    #   #{config.root}/app/presenters
    #   #{config.root}/app/decorators
    # )
    config.active_job.queue_adapter = :sidekiq
    Redis.exists_returns_integer = false

    config.logger = Logger.new("#{config.root}/log.log")
  end
end
