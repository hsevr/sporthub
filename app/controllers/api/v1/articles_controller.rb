module Api
  module V1
    class ArticlesController < BaseApiController
      before_action :set_article, only: [:show]

      def index
        articles = get_articles_service.call(request_params)

        json_response(ArticleSerializer.new(articles, article_includes), :ok)
      end

      def show
        json_response(ArticleSerializer.new(@article, article_includes), :ok)
      end

      private

      def request_params
        params.permit(
          :category,
          :sub_category,
          :team,
          :status,
          :q,
          :relationship
        )
      end

      def set_article
        @article = Admin::Article.find(params[:id])
      end

      def get_articles_service
        @get_articles_service ||= GetArticlesService.new
      end

      def article_includes
        @article_includes ||= { include: [:category, :sub_category, :team] }
      end
    end
  end
end