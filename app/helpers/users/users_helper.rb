# frozen_string_literal: true

module Users
  module UsersHelper
    def get_provider_url(provider)
      case provider
      when :facebook
        user_facebook_omniauth_authorize_path
      when :google_oauth2
        user_google_oauth2_omniauth_authorize_path
      end
    end

    def get_reverse_user_status(user)
      user.active? ? 'blocked' : 'active'
    end

    def get_reverse_user_status_for_view(user)
      user.active? ? 'block' : 'activate'
    end

    def get_full_name(user)
      user.first_name + ' ' + user.last_name
    end
  end
end
