module UserManagement
  class ChangeUserStatus
    include Interactor::Organizer

    organize ChangeStatus, SendMail
  end
end
