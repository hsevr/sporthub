# frozen_string_literal: true

module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      json_response({ message: e.message }, :not_found)
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      json_response({ message: e.message }, :unprocessable_entity)
    end

    rescue_from ApplicationApiError do |e|
      error_handler(e)
    end
  end

  def error_handler(e)
    code = e.config[:http_code] || 500
    render status: code, json: { success: false, error: e.message, code: e.code }
  end
end
