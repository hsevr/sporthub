# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'User management', :vcr, type: :feature do
  let(:admin) { create(:user, :admin, :active) }
  let(:admin_another) { create(:user, :admin, :active) }
  let(:user) { create(:user, :client, :active) }
  let(:user_blocked) { create(:user, :client, :blocked) }

  before { log_in admin }

  after { User.delete_all }

  describe 'Allow admin user to view user management page on the portal' do

    scenario 'Verify the lists of users on the Users page' do
      user
      expect(find('.list-of-users ul.nav-tabs').text).to have_content('users')
      expect(find('.list-of-users ul.nav-tabs').text).to have_content('admins')
      expect(find('.list-of-users ul.nav-tabs li.active').text).to have_content('users')
    end

    scenario 'Verify the content of Users tab on the Users page' do
      expect(find('.people #users thead').text).to have_content('Name')
      expect(find('.people #users thead').text).to have_content('Status')
      expect(find('.people #users thead').text).to have_content('Actions')
    end

    scenario 'Verify the content of Admins tab on the Users page', js: true do
      find('.admins-tab').click
      expect(find('.people .tab-pane.active thead').text).to have_content('Name')
      expect(find('.people .tab-pane.active thead').text).to have_content('Status')
      expect(find('.people .tab-pane.active thead').text).to have_content('Actions')
    end

  end

  describe 'Allow admin user to delete users on the portal', js: true do

    scenario 'Verify that admin can delete active users on the Users page' do
      load_users_page([user], 0)

      expect(page).to_not have_content(user.full_name)

      logout
      log_in user

      expect(page).to have_content('user_is_not_exist')
    end

    scenario 'Verify that admin can delete blocked users on the Users page' do
      load_users_page([user_blocked], 0)

      expect(page).to_not have_content(user_blocked.full_name)

      logout
      log_in user_blocked

      expect(page).to have_content('user_is_not_exist')
    end

    scenario 'Verify that admin can delete active admin on the Users page' do
      load_users_page([admin_another], 1, { admins_tab: true, with_confirm: true })

      expect(page).to_not have_content(admin_another.full_name)

      logout
      log_in admin_another

      expect(page).to have_content('user_is_not_exist')
    end

    scenario 'Verify that admin is not able to delete himself on the Users page' do
      load_users_page([], 0, { admins_tab: true, with_confirm: true })

      expect(page).to have_content(I18n.t('you_cannot_delete_the_current_user'))
    end

    scenario 'Verify that admin can cancel deleting users on the Users page' do
      load_users_page([admin_another], 1, { admins_tab: true, with_dismiss_confirm: true })

      expect(page).to have_content(admin_another.full_name)

      logout
      log_in admin_another
      visit admin_users_path

      expect(page).to have_content(I18n.t('users'))
    end
  end

  describe 'Allow admin user to delete users on the portal', js: true do
    scenario 'Verify that admin can cancel deleting users on the Users page' do
      load_users_page([admin_another], 1, { admins_tab: true })

      expect(page).to_not have_content('Block')
    end
  end

  describe 'Allow admin user to give user admin permissions on the portal', js: true do
    scenario 'Verify that admin can set admin permissions for active users on the Users page' do
      load_users_page([user], 0, {})

      click_link 'Make As Admin'

      expect(page).to_not have_content(admin_another.full_name)

      logout
      log_in user
      visit admin_users_path

      expect(page).to have_content(I18n.t('users'))
    end

    scenario 'Verify that admin is not able to set admin permissions for blocked users on the Users page' do
      load_users_page([user_blocked], 0, {})

      click_link 'Make As Admin'

      expect(page).to_not have_content('Make As Admin')
    end
  end

  describe 'Allow admin user to filter users on the portal', js: true do
    scenario 'Verify that admin can filter users' do
      load_users_page([user, user_blocked], nil, {})

      find('.people-filter a').click

      find('.filter-active').click

      expect(page).to have_content(user.full_name)
      expect(page).to_not have_content(user_blocked.full_name)

      find('.people-filter a').click
      find('.filter-blocked').click

      expect(page).to_not have_content(user.full_name)
      expect(page).to have_content(user_blocked.full_name)
    end
  end

  describe 'Allow admin user to search users on the portal', js: true do
    scenario 'Verify that admin can search users on the Users and Admins tabs' do
      load_users_page([user, user_blocked, admin_another], nil, {})

      fill_in('search', with: user.email)
      find('#search').native.send_key(:enter)

      expect(page).to have_content(user.full_name)
      expect(page).to_not have_content(user_blocked.full_name)

      find('.admins-tab').click

      fill_in('search', with: admin_another.email)
      find('#search').native.send_key(:enter)
      sleep 1
      find('.admins-tab').click

      expect(page).to have_content(admin_another.full_name)
      expect(page).to_not have_content(admin.full_name)
    end
  end

  def load_users_page(users, which_user, options = { with_confirm: true })
    visit admin_users_path

    find('.admins-tab').click if options[:admins_tab].present?
    all('.tab-pane.active .user-actions')[which_user].click if which_user.present?

    accept_confirm { click_link 'Delete' } if options[:with_confirm].present?
    dismiss_confirm { click_link 'Delete' } if options[:with_dismiss_confirm].present?
  end
end