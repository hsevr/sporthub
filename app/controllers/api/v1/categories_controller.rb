module Api
  module V1
    class CategoriesController < BaseApiController
      before_action :set_category, only: [:show]

      def index
        categories = Admin::Category.includes(sub_categories: :teams).all

        options = {
          include: [:sub_categories, :teams]
        }

        json_response(CategorySerializer.new(categories, options), :ok)
      end

      def show
        json_response(CategorySerializer.new(@category), :ok)
      end

      private

      def set_category
        @category = Admin::Category.find(params[:id])
      end
    end
  end
end