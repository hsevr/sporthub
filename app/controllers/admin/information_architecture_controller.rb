# frozen_string_literal: true

module Admin
  # Information Architecture Controller
  class InformationArchitectureController < BaseController
    before_action :set_title

    # GET /information_architecture
    def index
      @categories = Category.includes(:sub_categories).all
      @sub_categories = SubCategory.where(category_id: params[:category])
      @teams = Team.where(sub_category_id: params[:sub_category])

      @category = helpers.get_category_curr_params[:category]
      @sub_category = helpers.get_category_curr_params[:sub_category]
    end

    private

    def set_title
      @page_title = t('information_architecture')
    end
  end
end
