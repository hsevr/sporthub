class UserManagement::SendMail
  include Interactor

  SUCCESSFULLY_SEND_MAIL = 'Mail successfully send'.freeze
  FAIL_SEND_MAIL = 'Email not sent'.freeze

  def call
    if context.user.present? && context.params.present?
      if send_mail
        return context.message = SUCCESSFULLY_SEND_MAIL
      end
    end

    context.fail!(error: FAIL_SEND_MAIL)
  end

  private

  def send_mail()
    user = context.user.reload
    return UserMailer.with(user: user).change_permissions.deliver_later if context.params.key?(:is_admin)
    return UserMailer.with(user: user).user_status.deliver_later if context.params.key?(:status)
  end
end
