class CategorySerializer
  include JSONAPI::Serializer

  attributes :name, :order, :is_hidden
  has_many :sub_categories
  has_many :teams, through: :sub_categories
  has_many :articles
end
