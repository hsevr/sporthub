# frozen_string_literal: true

module Admin
  # UsersController
  class UsersController < BaseController
    before_action :set_user, only: %i[show edit update destroy change_permissions change_status]
    before_action :set_title

    def index
      @simple_users = get_user_service.call(params, 'simple_users')
      @admin_users = get_user_service.call(params.slice(:q), 'admins')

      @selected_user = user_service.get_selected_user(params, @simple_users, @admin_users)

      @tab = params[:tab] || 'users'
    end

    def update
      if @user.update(user_params)
        redirect_to admin_users_path, notice: t('user_was_successfully_updated.')
      else
        redirect_to admin_users_path, notice: t('user_was_not_updated.')
      end
    end

    def destroy
      if @user != current_user
        @user.destroy
        redirect_to admin_users_path, notice: t('user_was_successfully_destroyed.')
      else
        flash[:error] = t('you_cannot_delete_the_current_user.')
        redirect_to admin_users_path
      end
    end

    def change_permissions
      result = UserManagement::ChangeUserPermissions.call(user: @user, current_user: current_user,)

      if result.success?
        redirect_to admin_users_path, notice: t('user_was_successfully_change_permissions.')
      elsif result.error == UserManagement::SendMail::FAIL_SEND_MAIL
        redirect_to admin_users_path, notice: t('user_was_successfully_change_status_but_not_sent_a_email.')
      else
        redirect_to admin_users_path, notice: t('user_was_no_change_permissions.')
      end
    end

    def change_status
      result = UserManagement::ChangeUserStatus.call(
        current_user: current_user,
        user: @user,
        params: user_params
      )

      if result.success?
        flash[:notice] = t('user_was_successfully_change_status.')
      elsif result.error == UserManagement::SendMail::FAIL_SEND_MAIL
        flash[:error] = t('user_was_successfully_change_status_but_not_sent_a_email.')
      else
        flash[:error] = t('user_was_not_change_status.')
      end

      redirect_to admin_users_path
    end

    def export_users
      ExportWorker.perform_async(current_user.id, 'User', params[:type], 'users')
      flash[:notice] = t('export_started.')

      redirect_to admin_users_path
    end

    private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:first_name, :last_name, :status)
    end

    def user_service
      @user_service ||= UserService.new
    end

    def get_user_service
      @get_user_service ||= UserManager::GetUsersService.new
    end

    def set_title
      @page_title = t('users')
    end
  end
end
