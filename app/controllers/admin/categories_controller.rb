# frozen_string_literal: true

module Admin
  # Category controller
  class CategoriesController < BaseController
    before_action :set_category, only: %i[edit update destroy show]

    include ControllerParamsHelper, Pagy::Backend

    ARTICLE_PER_PAGE = 10.freeze

    # GET /categories/new
    def new
      @category = Category.new
    end

    # GET /category/1/edit
    def edit; end

    # POST /categories or /categories.json
    def create
      @category = Category.new(category_params)

      if @category.save
        redirect_to(
          admin_information_architecture_path(helpers.get_category_curr_params),
          notice: t('Category was successfully added.')
        )
      else
        render :new, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /categories/1 or /categories/1.json
    def update
      if @category.update(category_params)
        redirect_to(
          admin_information_architecture_path(helpers.get_category_curr_params),
          notice: t('Category was successfully added.')
        )
      else
        render :new, status: :unprocessable_entity
      end
    end

    # DELETE /categories/1 or /categories/1.json
    def destroy
      url_params = helpers.get_category_curr_params
      url_params.delete(:category) if url_params[:category] == @category.id.to_s

      @category.destroy

      redirect_to(
        admin_information_architecture_path(url_params),
        notice: t('Category was successfully destroyed.')
      )
    end

    # get /categories/1
    def show
      authorize Category
      set_title(@category.name)

      set_action_buttons_variables(
        button_name: '+ new article',
        button_url: new_admin_article_path(category: @category)
      ) if policy(Article).create?

      params[:category_id] = @category.id

      @articles = get_articles_service.call(params)

      Pagy::VARS[:items] = ARTICLE_PER_PAGE
      @pagy, @articles = pagy(@articles)

      @categories = Category.active
      @sub_categories = SubCategory.active
      @teams = Team.active
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def category_params
      params.require(:admin_category).permit(:name)
    end

    def category_service
      @category_service ||= Service::CategoryService.new(@category)
    end

    def get_articles_service
      @get_articles_service ||= GetArticlesService.new
    end

    def set_title(title = nil)
      @page_title = title || t('category')
    end
  end
end
