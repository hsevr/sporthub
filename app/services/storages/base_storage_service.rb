# frozen_string_literal: true

module Storages
  class BaseStorageService
    def upload(file_path)
      raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
    end

    def download(file_path, file_path_to)
      raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
    end
  end
end