# frozen_string_literal: true

require "google/cloud/storage"

module FileUploaders
  # Google cloud uploader service
  class UploadOnGoogleUploader < BaseUploader
    def initialize
      @storage = Google::Cloud::Storage.new
      @bucket = @storage.bucket ENV['GOOGLE_CLOUD_bucket']
    end

    def upload(file_path)
      @bucket.create_file file_path
    end
  end
end