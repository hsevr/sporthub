# frozen_string_literal: true

module Admin
  # Sub Category controller
  class SubCategoriesController < BaseController
    before_action :set_sub_category, only: %i[edit update destroy]
    # GET /sub_categories/new
    def new
      @sub_category = SubCategory.new
    end

    # GET /sub_category/1/edit
    def edit; end

    # POST /sub_categories
    def create
      @sub_category = SubCategory.new(sub_category_params)

      if @sub_category.save
        redirect_to(
          admin_information_architecture_path(helpers.get_category_curr_params),
          notice: t('Sub category was successfully added.')
        )
      else
        render :new, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /sub_categories/1
    def update
      if @sub_category.update(sub_category_params)
        redirect_to(
          admin_information_architecture_path(helpers.get_category_curr_params),
          notice: t('Sub category was successfully updated.')
        )
      else
        render :new, status: :unprocessable_entity
      end
    end

    # DELETE /sub_categories/1
    def destroy
      url_params = helpers.get_category_curr_params
      url_params.delete(:sub_category) if url_params[:sub_category] == @sub_category.id.to_s

      @sub_category.destroy

      redirect_to(
        admin_information_architecture_path(url_params),
        notice: t('sub category was successfully destroyed.')
      )
    end

    def move_to
      if @sub_category.update(sub_category_params)
        flash[:notice] = t('user_was_successfully_change_status.')
      else
        flash[:error] = t('user_was_not_change_status.')
      end

      redirect_to(admin_information_architecture_path(helpers.get_category_curr_params))
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_sub_category
      @sub_category = SubCategory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sub_category_params
      params.require(:admin_sub_category).permit(:name, :category_id, :is_hidden, :order)
    end

    def category_service
      @category_service ||= Service::CategoryService.new(@category)
    end
  end
end
