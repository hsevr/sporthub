# frozen_string_literal: true

module Api
  module V1
    class BaseApiController < ActionController::API
      include ::Response
      include ::ExceptionHandler

      before_action :authenticate_request
      attr_reader :current_user

      private

      def authenticate_request
        @current_user = AuthorizationApiService.call(request.headers)
        render json: { error: 'Not Authorized' }, status: 401 unless current_user
      end
    end
  end
end