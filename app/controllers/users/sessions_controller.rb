# frozen_string_literal: true

module Users
  class SessionsController < Devise::SessionsController
    # before_action :configure_sign_in_params, only: [:create]

    # GET /resource/sign_in
    # def new
    #   super
    # end

    # POST /resource/sign_in
    def create
      user = User.find_by(email: user_params[:email])
      flash[:error] = t('user_is_not_exist.') unless user
      super
    end

    # DELETE /resource/sign_out
    def destroy
      current_user.update_attribute(:last_sign_in_at, Time.now)
      super
    end

    private

    def user_params
      params.require(:user).permit(:email)
    end
  end
end
