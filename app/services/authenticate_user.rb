# frozen_string_literal: true

class AuthenticateUser < BaseService
  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  attr_accessor :email, :password

  def user
    user = User.find_by_email(email)
    return user if user&.valid_password?(password)

    raise ApplicationApiError::InvalidCredentials
  end
end