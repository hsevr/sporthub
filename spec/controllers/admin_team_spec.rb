# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Admin::TeamsController, type: :controller do
  let(:user) { create(:user, :admin, :active) }
  let(:category) { create(:category, :show) }
  let(:sub_category) { create(:sub_category, :show, category: category) }
  let(:team) { create(:team, :show, sub_category: sub_category) }
  let(:team_attributes) { attributes_for(:team, sub_category_id: sub_category) }

  before do
    sign_in user
  end

  describe '#create' do
    context 'with valid team params' do
      it 'creates a new team' do
        post :create, params: { admin_team: team_attributes }
        expect(Admin::Team.exists?(assigns(:team).id)).to eq(true)
      end
    end

    context 'with invalid params' do
      it 'raise error' do
        expect { post :create }.to raise_error(ActionController::ParameterMissing)
      end
    end
  end

  describe '#update' do
    context 'with valid params' do
      it 'Update the category' do
        patch :update, params: { id: team, admin_team: { name: 'new name' } }
        expect(assigns(:team).name).to eq('new name')
      end
    end
    context 'with invalid params' do
      it 'raise_error record not found' do
        expect { patch :update, params: { id: 123, admin_team: { name: 'new name' } } }.
          to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe '#destroy' do
    context 'with valid params' do
      it 'Destroy the category' do
        delete :destroy, params: { id: team }
        expect(Admin::Team.exists?(team.id)).to eq(false)
      end
    end
    context 'with invalid params' do
      it 'raise_error record not found' do
        expect { delete :destroy, params: { id: 1 } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
