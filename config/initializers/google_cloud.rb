require "google/cloud/storage"

Google::Cloud::Storage.configure do |config|
  config.project_id  = ENV['GOOGLE_CLOUD_PROJECT_ID']
  config.credentials = ENV['GOOGLE_CLOUD_CREDENTIAL']
end
