# frozen_string_literal: true

module FileUploaders
  class BaseUploader
    def upload(file_path)
      raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
    end
  end
end