# frozen_string_literal: true

module Admin
  module ControllerParamsHelper
    def set_action_buttons_variables(args)
      @action_button_name = args[:button_name]
      @action_button_url = args[:button_url]

      @action_cancel_button_name = args[:cancel_button_name]

      @action_btn_save = args[:btn_save]
    end
  end
end