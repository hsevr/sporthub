# frozen_string_literal: true

module Api
  module V1
    class AuthenticationController < BaseApiController
      skip_before_action :authenticate_request

      def authenticate
        token = AuthenticateUser.call(params[:email], params[:password])

        if token.present?
          render json: { auth_token: token }
        else
          render json: { error: 'invalid credentials' }, status: :unauthorized
        end
      end
    end
  end
end