class ExportMessageChannel < ApplicationCable::Channel
  def subscribed
    stream_from "export_message_channel"
  end

  def unsubscribed
  end
end
