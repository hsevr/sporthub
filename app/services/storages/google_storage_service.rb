# frozen_string_literal: true

module Storages
  class GoogleStorageService < BaseStorageService
    def initialize
      @storage = Google::Cloud::Storage.new
      @bucket = @storage.bucket ENV['GOOGLE_CLOUD_bucket']
    end

    def upload(file_path)
      @bucket.create_file file_path if File.exist?(file_path)
    end

    def download(file_path, file_path_to)
      file = @bucket.file(file_path)
      file.download(file_path_to) if file&.exists?
    end
  end
end