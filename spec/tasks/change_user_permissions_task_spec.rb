require 'spec_helper'

Rails.application.load_tasks

describe 'ChangeUserPermissionsTask' do
  let(:admin) { create(:user, :admin, :active) }
  let(:user_client) { create(:user, :client, :active) }

  after { Rake::Task["update_user_permissions"].reenable }

  context 'with correct params' do
    it 'should change status to client' do
      Rake::Task["update_user_permissions"].invoke(admin.email, 0)
      expect(admin.reload.is_admin).to eq(false)
    end

    it 'should change status to admin' do
      Rake::Task["update_user_permissions"].invoke(user_client.email, 1)
      expect(user_client.reload.is_admin).to eq(true)
    end
  end

  context 'with incorrect params' do
    it 'should not find user' do
      output = capture_rake_task_output(task_name: 'update_user_permissions', params: ['test@user.com', 0])
      expect(output).to eq("user do not found\n")
    end

    it 'should return message to add the params' do
      output = capture_rake_task_output(task_name: 'update_user_permissions', params: [])
      expect(output).to eq("please add the params\n")
    end
  end
end