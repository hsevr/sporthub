require 'spec_helper'

RSpec.describe UserManagement::SendMail, type: :interactor do
  let(:admin) { create(:user, :admin, :active) }
  let(:params) { { status: 'active' } }
  subject (:change_permissions) { UserManagement::SendMail.call(
    user: admin,
    params: params
  ) }

  describe '.call' do
    context 'with parameter - status' do
      it 'should send mail' do
        expect(change_permissions.success?)
          .to eq(true)
      end
    end

    context 'with parameter - is_admin' do
      let(:params) { { is_admin: false } }

      it 'should send mail' do
        expect(change_permissions.success?)
          .to eq(true)
      end
    end

    context 'without parameters' do
      let(:params) { { } }

      it 'should not send mail' do
        expect(change_permissions.success?)
          .to eq(false)
      end
    end

    context 'with do not existed user' do
      it 'should raise error' do
        admin.destroy
        expect { change_permissions.message }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
