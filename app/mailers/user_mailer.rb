# frozen_string_literal: true

class UserMailer < ApplicationMailer
  default from: 'sportHub@sportHub.com'

  def change_permissions
    @user = params[:user]
    mail(to: @user.email, subject: 'Changed permissions')
  end

  def user_status
    @user = params[:user]
    mail(to: @user.email, subject: 'Account status changed')
  end
end
