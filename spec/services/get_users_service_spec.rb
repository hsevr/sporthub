require 'spec_helper'

describe 'GetUsersService' do
  let(:get_users_service) { UserManager::GetUsersService.new }
  let(:admin) { create(:user, :admin, :active) }

  describe '#call' do
    context 'different input params' do
      it 'all users' do
        expect(get_users_service.call({})).to eq(User.all)
      end

      it 'admin users' do
        expect(get_users_service.call({}, 'admins')).to eq(User.admin)
      end

      it 'client users' do
        expect(get_users_service.call({}, 'simple_users')).to eq(User.simple_user)
      end

      it 'filter admin' do
        expect(get_users_service.call({ filter: 'admin' })).to eq(User.admin)
      end

      it 'filter simple user' do
        expect(get_users_service.call({ filter: 'simple_user' })).to eq(User.simple_user)
      end

      it 'filter online' do
        expect(get_users_service.call({ filter: 'online' })).to eq(User.online)
      end

      it 'filter offline' do
        expect(get_users_service.call({ filter: 'offline' })).to eq(User.offline)
      end

      it 'search users' do
        expect(get_users_service.call({ q: admin.email }).first.email)
          .to eq(admin.email)
      end
    end
  end
end