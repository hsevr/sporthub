Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    users: 'users/users',
    omniauth_callbacks: 'users/omniauth_callbacks'
  }

  root "admin/users#index"
  scope "(:locale)", locale: /#{I18n.available_locales.join("|")}/ do
    namespace :admin do
      root 'home_page#index', as: 'admin_home'

      resources :users, except: %i[show]
      patch 'users/change_permissions/:id', to: 'users#change_permissions', as: 'change_user_permissions'
      patch 'users/change_status/:id', to: 'users#change_status', as: 'change_user_status'
      get 'users/export', to: 'users#export_users', as: 'export_users'

      resources :categories
      resources :sub_categories
      resources :teams
      resources :articles

      get 'information-architecture', to: 'information_architecture#index'

      get 'sport-live/teams', to: 'sport_live_bar#teams'
      get 'sport-live/players', to: 'sport_live_bar#players'

      root "users#index"
    end
  end

  draw(:api)
end
