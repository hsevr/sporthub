# frozen_string_literal: true

# User service
class UserService
  def get_selected_user(params, simple_users, admin_users)
    return User.find(params[:selected_user]) if params[:selected_user].present?

    simple_users.first || admin_users.first
  end
end

