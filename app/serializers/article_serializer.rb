class ArticleSerializer
  include JSONAPI::Serializer
  attributes :alt, :headline, :caption, :location, :status
  belongs_to :category
  belongs_to :sub_category
  belongs_to :team
end
