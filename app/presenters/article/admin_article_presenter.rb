module Article
  class AdminArticlePresenter
    def sub_categories
      Admin::SubCategory.active
    end

    def teams
      Admin::Team.active
    end

    def new_article
      Admin::Article.new
    end
  end
end