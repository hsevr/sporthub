# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.field_filter(filter)
    self.public_send(filter)
  end
end
