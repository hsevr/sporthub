# == Schema Information
#
# Table name: teams
#
#  id              :uuid             not null, primary key
#  name            :string
#  sub_category_id :uuid             not null
#  order           :integer          default(0)
#  is_hidden       :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Admin::Team < ApplicationRecord
  belongs_to :sub_category

  validates :name, presence: true
  validates :name, length: { minimum: 2 }
  validates :sub_category, presence: true

  scope :active, -> { where(is_hidden: false) }
  scope :hidden, -> { where(is_hidden: true) }
end
