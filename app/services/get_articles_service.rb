# frozen_string_literal: true

class GetArticlesService < BaseService
  DEFAULT_RELATIONSHIP = "includes".freeze

  def call(params = {})
    query = Admin::Article
              .public_send(set_relationship(params), :sub_category)
              .public_send(set_relationship(params), :team)
              .with_attached_picture # .with_rich_text_content

    query = query.where(category_id: params[:category]) if params.key?(:category_id)
    query = query.where(sub_category_id: params[:sub_category]) if params.key?(:sub_category)
    query = query.where(team_id: params[:team]) if params.key?(:team)
    query = query.where(status: params[:status]) if params.key?(:status)
    query = query.search_by_fields(params[:q], search_fields) unless params[:q].nil?

    query
  end

  private

  def search_fields
    %i[headline caption]
  end

  def set_relationship(params)
    return DEFAULT_RELATIONSHIP unless ['includes', 'preload', 'eager_load'].include?(params[:relationship])

    params[:relationship]
  end
end