require 'spec_helper'

describe 'ExporterPDF' do
  include_context 'exporter_common'

  describe '#export' do
    context 'Check if files exists and are files' do
      it 'should create pdf' do
        ExportService.call(all_users, 'pdf', file_path_pdf)
        expect(file_path_pdf).to be_an_existing_file
      end
    end

    context 'Check if files with correct content' do
      it 'should correct content in pdf' do
        ExportService.call(all_users, 'pdf', file_path_pdf)

        text = ''
        PDF::Reader.new(file_path_pdf).pages.each do |page|
          text += page.text
        end

        all_users.each do |user|
          expect(text).to match user.email
        end
      end
    end
  end
end