desc 'The task changing user permissions'
task :update_user_permissions, [:user_email, :is_admin] => :environment do |t, args|
  if args[:user_email].nil? || args[:is_admin].nil?
    puts 'please add the params'
    next
  end

  user = User.find_by(email: args[:user_email])

  if user.present?
    user.update(is_admin: args[:is_admin])
    puts 'user permissions are changed'
  else
    puts 'user do not found'
  end
end
