# == Schema Information
#
# Table name: sub_categories
#
#  id          :uuid             not null, primary key
#  name        :string
#  category_id :uuid             not null
#  order       :integer          default(0)
#  is_hidden   :boolean          default(FALSE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
FactoryBot.define do
  factory :sub_category, class: 'Admin::SubCategory' do
    name { Faker::Name.name }
    association :category
    order { 0 }

    trait :hidden do
      is_hidden { true }
    end

    trait :show do
      is_hidden { false }
    end
  end
end
