# == Schema Information
#
# Table name: categories
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  order      :integer          default(0)
#  is_hidden  :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :category, class: 'Admin::Category' do
    name { Faker::Name.name }
    order { 0 }

    trait :hidden do
      is_hidden { true }
    end

    trait :show do
      is_hidden { false }
    end
  end
end
