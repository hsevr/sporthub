# frozen_string_literal: true

module Externals
  module SportHubApi
    module Teams
      def get_teams
        request('teams')
      end

      def get_team_statistics
        request('teams/statistics')
      end

      def get_team_seasons
        request('teams/seasons')
      end
    end
  end
end