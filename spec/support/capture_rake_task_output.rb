def capture_rake_task_output(task_name:, params:)
  stdout = StringIO.new
  $stdout = stdout
  Rake::Task[task_name].invoke(*params)
  $stdout = STDOUT
  Rake.application[task_name].reenable
  return stdout.string
end