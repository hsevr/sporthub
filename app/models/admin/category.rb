# == Schema Information
#
# Table name: categories
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  order      :integer          default(0)
#  is_hidden  :boolean          default(FALSE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Admin::Category < ApplicationRecord
  has_many :sub_categories, dependent: :destroy
  has_many :teams, through: :sub_categories
  has_many :articles

  validates :name, presence: true
  validates :name, length: { minimum: 2 }

  scope :active, -> { where(is_hidden: false) }
  scope :hidden, -> { where(is_hidden: true) }
end
