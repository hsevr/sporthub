class SubCategorySerializer
  include JSONAPI::Serializer

  attributes :name, :order, :is_hidden
  belongs_to :category
  has_many :teams
end
