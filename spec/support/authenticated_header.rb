def authenticated_header(user)
  { Authorization: JsonWebToken.encode(user_id: user.id) }
end
