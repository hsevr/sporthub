module UserManagement
  class ChangeUserPermissions
    include Interactor::Organizer

    organize ChangePermissions, SendMail
  end
end
