require 'spec_helper'

RSpec.describe UserManagement::ChangePermissions, type: :interactor do
  let(:admin) { create(:user, :admin, :active) }
  let(:user_client) { create(:user, :client, :active) }
  let(:user_client_blocked) { create(:user, :client, :blocked) }
  let(:params) { { status: 'blocked' } }
  subject (:change_permissions) { UserManagement::ChangePermissions.call(
    current_user: admin,
    user: user_client,
    params: params
  ) }
  subject (:change_permissions_not_admin) { UserManagement::ChangePermissions.call(
    current_user: user_client,
    user: user_client_blocked,
    params: params
  ) }

  describe '.call' do
    context 'current user is admin' do
      it 'should succeeded change permissions' do
        expect(change_permissions.success?).to eq(true)
      end

      it 'should change permissions to admin' do
        expect(change_permissions.user.is_admin).to eq(true)
      end
    end

    context 'current user is not admin' do
      it 'should failed change permissions' do
        expect(change_permissions_not_admin.success?).to eq(false)
      end
    end
  end
end
