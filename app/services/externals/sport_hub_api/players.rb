# frozen_string_literal: true

module Externals
  module SportHubApi
    module Players
      def get_players
        request('players')
      end

      def get_players_seasons
        request('players/seasons')
      end

      def get_players_topscorers
        request('teams/topscorers')
      end

      def get_players_topassists
        request('teams/topassists')
      end

      def get_players_topyellowcards
        request('teams/topyellowcards')
      end

      def get_players_topredcards
        request('teams/topredcards')
      end
    end
  end
end