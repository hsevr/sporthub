# frozen_string_literal: true

module Admin
  class CategoryPolicy < ApplicationPolicy
    def show?
      !user.customer?
    end
  end
end

