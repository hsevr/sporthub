namespace :api do
  namespace :v1 do
    post 'authenticate', to: 'authentication#authenticate'
    resources :articles, only: %i[index show]
    resources :categories, only: %i[index show]
  end
end
