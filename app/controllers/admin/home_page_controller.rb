# frozen_string_literal: true
module Admin
  #Home page controller
  class HomePageController < BaseController

    def index; end

    private

    def set_title
      @page_title = t('home')
    end
  end
end
