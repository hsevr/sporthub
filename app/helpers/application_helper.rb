# frozen_string_literal: true
#
module ApplicationHelper
  include Pagy::Frontend

  def devise_mapping
    Devise.mappings[:user]
  end

  def resource_name
    devise_mapping.name
  end

  def resource_class
    devise_mapping.to
  end

  def admin_namespace?
    controller.class.name.split('::').first == 'Admin'
  end
end
