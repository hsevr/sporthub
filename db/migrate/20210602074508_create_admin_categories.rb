class CreateAdminCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :categories, id: :uuid  do |t|
      t.string :name, null: false
      t.integer :order, default: 0
      t.boolean :is_hidden, default: false

      t.timestamps
    end
  end
end
