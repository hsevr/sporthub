require 'spec_helper'

describe 'ExportService' do
  let(:exporter_csv) { ExporterCSV }
  let(:exporter_xlsx) { ExporterXLSX }
  let(:exporter_pdf) { ExporterPDF }

  describe '#call' do
    context 'Check which exporter called' do
      it 'should called csv' do
        expect_any_instance_of(ExporterCSV).to receive(:export)
        ExportService.call(nil, 'csv', '/')
      end

      it 'should called xlsx' do
        expect_any_instance_of(ExporterXLSX).to receive(:export)
        ExportService.call(nil, 'xlsx', '/')
      end

      it 'should called pdf' do
        expect_any_instance_of(ExporterPDF).to receive(:export)
        ExportService.call(nil, 'pdf', '/')
      end
    end

    context 'Check raise errors' do
      it 'should return raise error' do
        expect { ExportService.call(nil, 'notExist', '/') }.to raise_error(RuntimeError)
      end
    end
  end
end