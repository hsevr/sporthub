# == Schema Information
#
# Table name: articles
#
#  id              :uuid             not null, primary key
#  alt             :string           not null
#  headline        :string           not null
#  caption         :string           not null
#  location        :string           not null
#  comments        :boolean          default(FALSE)
#  status          :integer          default("unpublish")
#  category_id     :uuid
#  sub_category_id :uuid
#  team_id         :uuid
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
FactoryBot.define do
  factory :article, class: 'Admin::Article' do
    alt { 'alt' }
    headline { 'headline' }
    caption { 'caption' }
    location { 'location' }
    association :category
    association :sub_category
    association :team

    trait :with_comments do
      with_comments { true }
    end

    trait :without_comments do
      with_comments { false }
    end

    trait :published do
      status { 1 }
    end

    trait :unpublish do
      status { 0 }
    end
  end
end
