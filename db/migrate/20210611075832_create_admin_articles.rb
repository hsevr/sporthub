class CreateAdminArticles < ActiveRecord::Migration[6.1]
  def change
    create_table :articles, id: :uuid do |t|
      t.string :alt, null: false
      t.string :headline, null: false
      t.string :caption, null: false
      t.string :location, null: false
      t.boolean :with_comments, default: false
      t.integer :status, default: 0, limit: 2
      t.belongs_to :category, foreign_key: true, type: :uuid
      t.belongs_to :sub_category, foreign_key: true, type: :uuid
      t.belongs_to :team, foreign_key: true, type: :uuid
      t.timestamps
    end
  end
end
