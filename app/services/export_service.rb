# frozen_string_literal: true

class ExportService < BaseService
  def initialize(data, type, file_location, params = nil)
    @data = data
    @file_location = file_location
    @params = params
    if Object.const_defined?("Exporter" + type.upcase)
      @exporter = "Exporter#{ type.upcase }".safe_constantize.new
    else
      raise 'exporter is not find'
    end
  end

  def call
    @exporter.export(@data, @file_location)
  end

  private

  attr_writer :data, :file_location, :exporter
end