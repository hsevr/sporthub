require 'spec_helper'

describe 'GoogleStorageService' do
  let(:dir_path) { "#{Rails.root}/spec/test_files/" }
  let(:file_path) { "#{Rails.root}/spec/test_files/test.txt" }
  let(:file_path_to) { "#{Rails.root}/spec/test_files/google_test.txt" }
  let(:file_not_exist_path) { "#{Rails.root}/spec/test_files/test_not.txt" }
  let(:google_cloud_storage) { double('GoogleCloud') }
  let(:google_cloud_storage_bucket) { double('GoogleCloudBucket') }

  subject { Storages::GoogleStorageService.new }

  before do
    Dir.mkdir(dir_path) unless directory?(dir_path)
    File.open(file_path, "w")

    allow(Google::Cloud::Storage).to receive(:new) { google_cloud_storage }
    allow(google_cloud_storage).to receive(:bucket) { google_cloud_storage_bucket }
  end

  after do
    FileUtils.rm_rf(dir_path)
  end

  describe '#initialize' do
    context 'Check if the bucket method called' do
      it 'should upload file' do
        expect(google_cloud_storage).to receive(:bucket)
        subject
      end
    end
  end

  describe '#upload' do
    context 'Check if the upload method called' do
      it 'should upload file' do
        allow(google_cloud_storage_bucket).to receive(:create_file)

        expect(google_cloud_storage_bucket).to receive(:create_file).with(file_path)
        subject.upload(file_path)
      end
    end

    context 'Check if the file exist' do
      it 'file not exist' do
        expect(subject.upload(file_not_exist_path)).to eq(nil)
      end
    end
  end

  describe '#download' do
    context 'Check if the download method called' do
      it 'file should downloaded' do
        allow(google_cloud_storage_bucket).to receive(:file)

        expect(google_cloud_storage_bucket).to receive(:file)
        subject.download(file_path, file_path_to)
      end

      it 'file not exist' do
        allow(google_cloud_storage_bucket).to receive(:file)

        expect(subject.download(file_not_exist_path, file_not_exist_path)).to eq(nil)
      end
    end
  end
end