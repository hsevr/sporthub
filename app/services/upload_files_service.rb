# frozen_string_literal: true

# Upload files service
class UploadFilesService < BaseService
  def initialize(file_path)
    @file_path = file_path

    if Object.const_defined?("FileUploaders::UploadOn#{ENV['APP_STORAGE']}Uploader")
      @storage = "FileUploaders::UploadOn#{ENV['APP_STORAGE']}Uploader".safe_constantize.new
    else
      raise 'Storage is not find'
    end
  end

  def call
    @storage.upload(@file_path)
  end
end