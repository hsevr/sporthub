require "google/cloud/storage"

desc 'The task import users from csv file to DB'
task :import_users, [:file_path, :storage] => :environment do |t, args|
  if args[:file_path].nil?
    puts 'please add the params'
    next
  end

  full_path = "#{Rails.root}/#{args[:file_path]}"

  if args[:storage]&.include?('google')
    storage = Storages::GoogleStorageService.new
    storage.download(args[:file_path], full_path)
  end

  unless File.exist?(full_path)
    puts 'file not found'
    next
  end

  users = []

  CSV.foreach(full_path, headers: true) do |row|
    users << row.to_h
  end

  begin
    User.insert_all(users)
  rescue => e
    puts "Something went wrong!"
    Rails.logger.debug "import error: #{e.message}"
  end
end
