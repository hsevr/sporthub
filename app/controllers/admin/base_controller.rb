# frozen_string_literal: true

module Admin
  class BaseController < ActionController::Base
    include Pundit

    around_action :switch_locale
    protect_from_forgery with: :exception
    before_action :ensure_admin_user!
    before_action :set_title


    layout 'application'

    rescue_from Pundit::NotAuthorizedError do
      redirect_to root_url, alert: 'You do not have access to this page'
    end

    def ensure_admin_user!
      unless current_user && current_user.is_admin
        redirect_to root_path, error: t('you_did_not_have_such_permissions_for_that')
      end
    end

    def set_title
      @page_title = t('Sport Hub')
    end

    def default_url_options
      { locale: I18n.locale }
    end

    def switch_locale(&action)
      locale = params[:locale] || I18n.default_locale
      I18n.with_locale(locale, &action)
    end
  end
end
