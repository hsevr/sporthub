# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Users::RegistrationsController, type: :controller do
  let(:user) { create(:user) }
  let(:user_attributes) { attributes_for(:user) }

  before do
    request.env['devise.mapping'] = Devise.mappings[:user]
  end

  context 'Register#create' do
    it 'create a new user' do
      post :create, params: { user: user_attributes }
      expect(User.exists?(assigns(:user).id)).to eq(true)
    end

    it 'user already used email address' do
      user_attributes[:email] = user.email

      post :create, params: { user: user_attributes }
      expect(assigns(:user).errors.full_messages).to eq(['Email has already been taken'])
    end

    it 'user empty required fields' do
      post :create, params: { user: build(:user) }
      expect(assigns(:user).errors.full_messages).to eq(["Email can't be blank", "Password can't be blank",
                                                         "First name can't be blank", "Last name can't be blank"])
    end

    it 'user invalid password' do
      user_attributes[:password] = '12'

      post :create, params: { user: user_attributes }
      expect(assigns(:user).errors.full_messages).to eq(['Password is too short (minimum is 3 characters)'])
    end

    it 'user invalid email' do
      user_attributes[:email] = 'test'

      post :create, params: { user: user_attributes }
      expect(assigns(:user).errors.full_messages).to eq(['Email is invalid'])
    end
  end

  context 'Register#update' do
    it 'update password' do
      sign_in user
      password = '156S@Febveqe51'

      put :update, params: { user: { password: password } }
      expect(BCrypt::Password.new(assigns(:user).encrypted_password)).to_not eq(user.password)
    end
  end
end
