module Searchable
  extend ActiveSupport::Concern

  module ClassMethods
    def search_by_fields(q, fields)
      sql = fields.map { |field| "#{field} LIKE ?" }.join(' OR ')
      parameters = fields.map { "%#{q}%" }
      where(sql, *parameters)
    end
  end
end