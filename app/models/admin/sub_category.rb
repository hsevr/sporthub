# == Schema Information
#
# Table name: sub_categories
#
#  id          :uuid             not null, primary key
#  name        :string
#  category_id :uuid             not null
#  order       :integer          default(0)
#  is_hidden   :boolean          default(FALSE)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Admin::SubCategory < ApplicationRecord
  belongs_to :category
  has_many :teams, dependent: :destroy

  validates :name, presence: true
  validates :name, length: { minimum: 2 }
  validates :category_id, presence: true

  scope :active, -> { where(is_hidden: false) }
  scope :hidden, -> { where(is_hidden: true) }
end
