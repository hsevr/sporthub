RSpec.shared_context 'exporter_common' do
  let!(:user_client) { create(:user, :client, :active) }
  let(:all_users) { User.all }
  let(:dir_path) { "#{Rails.root}/spec/test_files/" }
  let(:file_path_csv) { "#{Rails.root}/spec/test_files/test.csv" }
  let(:file_path_xlsx) { "#{Rails.root}/spec/test_files/test.xlsx" }
  let(:file_path_pdf) { "#{Rails.root}/spec/test_files/test.pdf" }

  before do
    Dir.mkdir(dir_path) unless directory?(dir_path)
  end

  after do
    FileUtils.rm_rf(dir_path)
  end
end