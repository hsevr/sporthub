class TeamSerializer
  include JSONAPI::Serializer
  attributes :name, :order, :is_hidden
end
