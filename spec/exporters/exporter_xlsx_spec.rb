require 'spec_helper'

describe 'ExporterXLSX' do
  include_context 'exporter_common'

  describe '#export' do
    context 'Check if files exists and are files' do
      it 'should create xlsx' do
        ExportService.call(all_users, 'xlsx', file_path_xlsx)
        expect(file_path_xlsx).to be_an_existing_file
      end
    end

    context 'Check if files with correct content' do
      it 'should correct content in xlsx' do
        ExportService.call(all_users, 'xlsx', file_path_xlsx)
        all_users.each do |user|
          expect(Roo::Spreadsheet.open(file_path_xlsx).to_s).to match user.email
        end
      end
    end
  end
end