class ExportWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'export', retry: 2

  def perform(user_id, model, export_type, file_name)
    if user_id.empty? || export_type.empty? || file_name.empty?
      logger.error('parameters cannot be empty')
      return
    end

    user = User.find(user_id)

    if !user.present?
      logger.error('User is not find')
      return
    end

    dir_exporter_path = "#{ ENV['EXPORT_PATH'] }#{ export_type }/"
    dir_path = "#{ ENV['EXPORT_PATH'] }#{ export_type }/#{user_id}"
    file_location = "#{dir_path}/#{file_name}.#{export_type}"
    Dir.mkdir(dir_exporter_path) unless File.directory?(dir_exporter_path)
    Dir.mkdir(dir_path) unless File.directory?(dir_path)
    # Here only export example.
    # If we need more we can change query
    if Object.const_defined?(model)
      data = model.safe_constantize.all
    else
      logger.error('model is not find')
      return
    end

    ExportService.call(data, export_type, file_location)
    UploadFilesService.call(file_location)

    ExportMailer.export(user, file_location, export_type).deliver

    ActionCable.server.broadcast("export_message_channel", html: 'The export was successful')
  end
end
