module Admin
  class ArticlesController < BaseController
    before_action :set_article, only: %i[ show edit update destroy ]

    include ControllerParamsHelper

    # GET /admin/articles or /admin/articles.json
    def index
      @articles = Article.all
    end

    # GET /admin/articles/1 or /admin/articles/1.json
    def show; end

    # GET /admin/articles/new
    def new
      @data = ::Article::AdminArticlePresenter.new

      set_action_buttons_variables(
        button_name: 'save',
        button_url: '#',
        cancel_button_name: 'cancel',
        btn_save: true
      )
    end

    # GET /admin/articles/1/edit
    def edit
      @data = ::Article::AdminArticlePresenter.new

      set_action_buttons_variables(
        button_name: 'save',
        button_url: '#',
        cancel_button_name: 'cancel',
        btn_save: true
      )
    end

    # POST /admin/articles or /admin/articles.json
    def create
      @article = Admin::Article.new(article_params)
      if @article.save
        redirect_to(
          admin_category_path(article_params[:category_id]),
          notice: "Article was successfully created."
        )
      else
        flash[:danger] = @article.errors.full_messages
        redirect_back(fallback_location: admin_articles_path(category: article_params[:category_id]))
      end
    end

    # PATCH/PUT /admin/articles/1 or /admin/articles/1.json
    def update
      if @article.update(article_params)
        redirect_to(admin_category_path(params[:category]), notice: "Article was successfully updated.")
      else
        render :edit, status: :unprocessable_entity
      end
    end

    # DELETE /admin/articles/1 or /admin/articles/1.json
    def destroy
      @article.destroy
      redirect_to(admin_category_path(params[:category]), notice: "Article was successfully destroyed.")
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def article_params
      params.require(:admin_article).permit(
        :alt,
        :headline,
        :caption,
        :content,
        :location,
        :comments,
        :picture,
        :category_id,
        :sub_category_id,
        :team_id,
        :status
      )
    end

  end
end
