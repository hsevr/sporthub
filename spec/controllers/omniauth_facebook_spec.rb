# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Users::OmniauthCallbacksController, type: :controller do
  let(:user) { create(:user) }

  before do
    request.env['devise.mapping'] = Devise.mappings[:user]
    request.env['omniauth.auth'] = OmniAuth::AuthHash.new({
                                                            provider: 'facebook',
                                                            uid: '123456789',
                                                            info: {
                                                              name: 'Tony Stark',
                                                              first_name: 'Alberto',
                                                              last_name: 'Pellizzon',
                                                              email: 'tony@stark.com'
                                                            },
                                                            credentials: {
                                                              token: 'token',
                                                              refresh_token: 'refresh token'
                                                            }
                                                          })
  end

  context 'OmniauthCallbacks#facebook' do
    it 'Sign in by Facebook' do
      get :facebook
      expect(controller.current_user).to have_attributes(email: 'tony@stark.com', uid: '123456789')
    end
  end
end
