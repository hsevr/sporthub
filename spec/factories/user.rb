# frozen_string_literal: true

FactoryBot.define do
  factory(:user) do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    password { Faker::Internet.password(min_length: 8) }

    trait :active do
      status { 1 }
    end

    trait :blocked do
      status { 0 }
    end

    trait :admin do
      is_admin { true }
    end

    trait :client do
      is_admin { false }
    end
  end
end
