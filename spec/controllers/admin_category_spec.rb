# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Admin::CategoriesController, type: :controller do
  let(:user) { create(:user, :admin, :active) }
  let(:category) { create(:category, :show) }
  let(:category_attributes) { attributes_for(:category) }

  before { sign_in user }


  describe '#create' do
    context 'with valid category params' do
      it 'creates a new category' do
        post :create, params: { admin_category: category_attributes }
        expect(Admin::Category.exists?(assigns(:category).id)).to eq(true)
      end
    end

    context 'with invalid params' do
      it 'raise error' do
        expect { post :create }.to raise_error(ActionController::ParameterMissing)
      end
    end
  end

  describe '#update' do
    context 'with valid params' do
      it 'Update the category' do
        patch :update, params: { id: category, admin_category: { name: 'new name' } }
        expect(assigns(:category).name).to eq('new name')
      end
    end
    context 'with invalid params' do
      it 'raise_error record not found' do
        expect { patch :update, params: { id: 123, admin_category: { name: 'new name' } } }.
          to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end

  describe '#destroy' do
    context '#destroy' do
      it 'Destroy the category' do
        delete :destroy, params: { id: category }
        expect(Admin::Category.exists?(category.id)).to eq(false)
      end
    end

    context 'with invalid params' do
      it 'raise_error record not found' do
        expect { delete :destroy, params: { id: 1 } }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
