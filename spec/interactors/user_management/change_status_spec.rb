require 'spec_helper'

RSpec.describe UserManagement::ChangeStatus, type: :interactor do
  let(:admin) { create(:user, :admin, :active) }
  let(:user_client) { create(:user, :client, :active) }
  let(:user_client_blocked) { create(:user, :client, :blocked) }
  let(:params) { { status: 'blocked' } }
  subject (:change_status) { UserManagement::ChangeStatus.call(
    current_user: admin,
    user: user_client,
    params: params
  ) }
  subject (:change_status_not_admin) { UserManagement::ChangeStatus.call(
    current_user: user_client,
    user: user_client_blocked,
    params: params
  ) }

  subject (:change_status_user_admin) { UserManagement::ChangeStatus.call(
    current_user: user_client,
    user: admin,
    params: params
  ) }

  describe '.call' do
    context 'current user is admin' do
      it 'should succeeded change status' do
        expect(change_status.success?).to eq(true)
      end

      it 'should change status to blocked' do
        expect(change_status.user.status).to eq('blocked')
      end
    end

    context 'current user is not admin' do
      it 'should failed change status' do
        expect(change_status_not_admin.success?).to eq(false)
      end
    end

    context 'changed user is admin' do
      it 'should failed change status' do
        expect(change_status_user_admin.success?).to eq(false)
      end
    end
  end
end
