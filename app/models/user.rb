# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id                     :uuid             not null, primary key
#  first_name             :string           not null
#  last_name              :string           not null
#  email                  :string           not null
#  encrypted_password     :string           not null
#  remember_created_at    :datetime
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  status                 :integer          default("active")
#  is_admin               :boolean          default(FALSE)
#  created_at             :datetime
#  updated_at             :datetime
#  provider               :string
#  uid                    :string
#
# User model
class User < ApplicationRecord

  enum status: { active: 1, blocked: 0 }
  enum role: { customer: 0, manager: 1, admin: 2 }

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable, :omniauthable, omniauth_providers: %i[facebook google_oauth2]

  validates :first_name, presence: true
  validates :last_name, presence: true

  include Searchable

  scope :admin, -> { where(is_admin: true) }
  scope :simple_user, -> { where(is_admin: false) }
  scope :online, -> { where('current_sign_in_at >= last_sign_in_at') }
  scope :offline, -> { where('current_sign_in_at < last_sign_in_at') }

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.first_name = auth.info.name
      user.last_name = auth.info.name
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session['devise.facebook_data'] &&
                session['devise.facebook_data']['extra']['raw_info'] &&
                user.email.blank?
        user.email = data['email']
      end
    end
  end

  def online?
    current_sign_in_at.present? ? current_sign_in_at >= last_sign_in_at : false
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def self.export_attributes
    %w{id email full_name}
  end
end
