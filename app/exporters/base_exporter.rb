class BaseExporter
  def export(data, file_location, params)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end