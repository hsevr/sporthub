# frozen_string_literal: true

module UserManager
  class GetUsersService < BaseService

    def call(params, user_type = nil)
      query = case user_type
              when 'admins'
                User.admin
              when 'simple_users'
                User.simple_user
              else
                User.where(nil)
              end

      query = query.search_by_fields(params[:q], search_fields) unless params[:q].nil?
      query = query.field_filter(params[:filter]) unless params[:filter].nil?

      query
    end

    private

    def search_fields
      %i[email first_name last_name]
    end
  end
end