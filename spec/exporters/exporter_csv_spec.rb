require 'spec_helper'

describe 'ExporterCSV' do
  include_context 'exporter_common'

  describe '#export' do
    context 'Check if files exists and are files' do
      it 'should create csv' do
        ExportService.call(all_users, 'csv', file_path_csv)
        expect(file_path_csv).to be_an_existing_file
      end
    end

    context 'Check if files with correct content' do
      it 'should correct content in csv' do
        ExportService.call(all_users, 'csv', file_path_csv)
        all_users.each do |user|
          expect(File.read(file_path_csv)).to match user.email
        end
      end
    end
  end
end