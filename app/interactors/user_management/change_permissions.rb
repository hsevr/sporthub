class UserManagement::ChangePermissions
  include Interactor

  def call
    if context.current_user.is_admin
      context.user.update(is_admin: !context.user.is_admin)
      context.params = { is_admin: !context.user.is_admin }
    else
      context.fail!
    end

    def rollback
      context.user.update(is_admin: context.user.is_admin)
    end
  end
end
