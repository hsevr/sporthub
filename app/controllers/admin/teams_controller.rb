# frozen_string_literal: true

module Admin
  # Category controller
  class TeamsController < BaseController
    before_action :set_team, only: %i[edit update destroy]
    # GET /teams/new
    def new
      @team = Team.new
    end

    # GET /team/1/edit
    def edit; end

    # POST /teams
    def create
      @team = Team.new(team_params)

      if @team.save
        redirect_to(
          admin_information_architecture_path(helpers.get_category_curr_params),
          notice: t('team_was_successfully_added.')
        )
      else
        render :new, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /teams/1
    def update
      if @team.update(team_params)
        redirect_to(
          admin_information_architecture_path(helpers.get_category_curr_params),
          notice: t('team_was_successfully_updated')
        )
      else
        render :new, status: :unprocessable_entity
      end
    end

    # DELETE /teams/1
    def destroy
      @team.delete
      redirect_to(
        admin_information_architecture_path(helpers.get_category_curr_params),
        notice: t('team was successfully destroyed.')
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def team_params
      params.require(:admin_team).permit(:name, :sub_category_id, :is_hidden, :order)
    end
  end
end
