require 'spec_helper'

describe 'GetArticlesService' do
  let(:category) { create(:category, :show) }
  let(:sub_category) { create(:sub_category, :show, category: category) }
  let(:team) { create(:team, :show, sub_category: sub_category) }
  let!(:article) { create(
    :article,
    category: category,
    sub_category: sub_category,
    team: team
  ) }
  let(:article_published) { create(
    :article,
    :published,
    category: category,
    sub_category: sub_category,
    team: team
  ) }
  let(:get_articles_service) { GetArticlesService.new }
  let(:all_articles) { Admin::Article.all }
  let(:all_unpublish_articles) { Admin::Article.where(status: 0) }

  describe '#call' do
    subject(:service) { get_articles_service.call }

    context 'different input params' do

      it 'should included the category' do
        expect(get_articles_service.call({ category: category }).first.category)
          .to eq(article.category)
      end

      it 'should return all category articles' do
        expect(service).to eq(all_articles)
      end

      it 'should return only published' do
        article_published
        expect(get_articles_service.call({ category: category, status: 1 }))
          .to eq([article_published])
      end

      it 'should return only unpublished' do
        expect(get_articles_service.call({ category: category, status: 0 }))
          .to eq(all_unpublish_articles)
      end

      it 'should included the sub_category' do
        expect(get_articles_service.call({ category: category, sub_category: sub_category }).first.sub_category)
          .to eq(article.sub_category)
      end

      it 'should included the team' do
        expect(get_articles_service.call({ category: category, team: team }).first.team)
          .to eq(article.team)
      end

      it 'should return searched articles' do
        expect(get_articles_service.call({ category: category, q: article.headline }).first.headline)
          .to eq(article.headline)
      end
    end
  end
end