# frozen_string_literal: true

module CategoriesHelper
  def get_category_curr_params
    { category: params[:category], sub_category: params[:sub_category] }
  end

  def get_categories
    Admin::Category.active
  end
end
