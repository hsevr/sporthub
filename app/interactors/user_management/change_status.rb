class UserManagement::ChangeStatus
  include Interactor
  include Users::UsersHelper
  # include ActionView::Helpers

  def call
    if context.current_user.is_admin && !context.user.is_admin
      context.user.update(context.params)
    else
      context.fail!
    end
  end

  def rollback
    context.user.update(status: get_reverse_user_status(context.user))
  end
end
