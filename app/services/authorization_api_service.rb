class AuthorizationApiService < BaseService
  def initialize(headers = {})
    @headers = headers
  end

  def call
    raise ApplicationApiError::InvalidToken unless decoded_auth_token

    User.find(decoded_auth_token[:user_id])
  end

  private

  attr_reader :headers

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  def http_auth_header
    raise ApplicationApiError::MissingToken unless headers['Authorization'].present?

    return headers['Authorization'].split(' ').last
  end

end