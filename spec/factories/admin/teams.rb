# == Schema Information
#
# Table name: teams
#
#  id              :uuid             not null, primary key
#  name            :string
#  sub_category_id :uuid             not null
#  order           :integer          default(0)
#  is_hidden       :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
FactoryBot.define do
  factory :team, class: 'Admin::Team' do
    name { Faker::Name.name }
    association :sub_category
    order { 0 }

    trait :hidden do
      is_hidden { true }
    end

    trait :show do
      is_hidden { false }
    end
  end
end
