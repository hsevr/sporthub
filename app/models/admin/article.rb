# == Schema Information
#
# Table name: articles
#
#  id              :uuid             not null, primary key
#  alt             :string           not null
#  headline        :string           not null
#  caption         :string           not null
#  location        :string           not null
#  comments        :boolean          default(FALSE)
#  status          :integer          default("unpublish")
#  category_id     :uuid
#  sub_category_id :uuid
#  team_id         :uuid
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Admin::Article < ApplicationRecord
  belongs_to :category
  belongs_to :sub_category
  belongs_to :team

  enum status: %i[unpublish published]

  validates :alt, presence: true
  validates :headline, presence: true
  validates :caption, presence: true
  validates :location, presence: true
  validates :category_id, presence: true
  validates :sub_category_id, presence: true
  validates :team_id, presence: true

  has_one_attached :picture
  has_rich_text :content

  include Searchable

  def country_name
    ISO3166::Country[location]
  end

end
