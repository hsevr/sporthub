# frozen_string_literal: true

module Admin
  class ArticlePolicy < ApplicationPolicy
    def create?
      user.admin?
    end
  end
end