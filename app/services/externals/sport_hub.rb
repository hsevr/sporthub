# frozen_string_literal: true

module Externals
  class SportHub
    include SportHubApi::Teams, SportHubApi::Players

    attr_accessor :params

    URL = 'https://v3.football.api-sports.io/'.freeze
    CACHE_KEY = 'SportHubApi'

    def initialize
      @connection = Faraday.new(url: URL) do |faraday|
        faraday.request :json
        faraday.response :json
        faraday.adapter Faraday.default_adapter
        faraday.headers['x-rapidapi-key'] = ENV['SPORT_HUB_API']
      end
    end

    def request(endpoint, method = 'get')
      cache = Rails.cache.fetch("#{CACHE_KEY}#{endpoint}")
      return cache if cache

      begin
        result = @connection.public_send(method, endpoint, params)

        return result.body unless result['errors'].present?

        Rails.cache.fetch("#{CACHE_KEY}#{endpoint}", expires_in: 30.minute) do
          result.body
        end
      rescue => e
        Rails.logger.debug "import error: #{e.message}"
        { errors: e.message }
      end
    end
  end
end