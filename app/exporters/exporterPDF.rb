# frozen_string_literal: true

require "prawn"

class ExporterPDF < BaseExporter
  def export(data, file_location, params = {})
    head = data.export_attributes

    Prawn::Document.generate(file_location) do
      font "#{Rails.root}/public/assets/fonts/DejaVuSans.ttf"
      text head.join("|")

      data.each do |row|
        text head.map{ |attr| row.public_send(attr) }.join("|")
      end
    end
  end
end